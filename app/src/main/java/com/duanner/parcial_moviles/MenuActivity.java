package com.duanner.parcial_moviles;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MenuActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        final EditText name_empresa = (EditText)findViewById(R.id.editText_menu);
        Button sendButton = (Button)findViewById(R.id.button_send);

        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = name_empresa.getText().toString();

                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);

                intent.putExtra("empresa", name);

                startActivity(intent);
            }
        });

    }

}
