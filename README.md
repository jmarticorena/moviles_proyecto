# Usando Cloud SQL #
* Grant access a todos: 0.0.0.0/0
* Pedir una IP v4
* Crear un usuario con host client : %

# Ejemplo de uso de los script #
* Insertar en la BD: http://gg-wp-easynoob.appspot.com/rutas/write/bd?nombre=test2&coordenadas=10.4,123.45;67.90,38.90
* Query a la BD: http://gg-wp-easynoob.appspot.com/rutas/read/bd?query=select * from rutas


```
#!linux

mysql --host=173.194.254.170 --user=dev --password
dev
```




```
#!mysql

CREATE DATABASE rutas;

CREATE TABLE rutas.rutas (
  id INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(50),
  coordenadas TEXT,
  PRIMARY KEY(entryID)
);
```